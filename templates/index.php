<DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>%TITLE%</title>

    <!-- Bootstrap -->
    <!-- <link href="css/style.css" rel="stylesheet"> -->
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
        
    <div class="container">
   <div><h2>Contact Form</h2></div>
        <div style="color: #FF0000; font-size: 15px;"><strong>%ERRORS%</strong></div>

  <form method="post" name="email">
    <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" id="name" value="%NAME%" name="name" placeholder="Enter your name">
  </div>
  <div class="form-group">
    <label for="email">Email address</label>
    <input type="email" class="form-control" id="email" value="%EMAIL%" name="email" aria-describedby="emailHelp" placeholder="Enter email">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
    <div class="form-group">
    <label for="subject">Subject</label>
    <select class="form-control" id="subject" name="subject" >
      <option value="1">Question</option>
      <option value="2">Comment</option>
      <option value="3">Consultation</option>
      <option value="4">Review</option>
    </select>
  </div>
    <div class="form-group">
    <label for="message">Your message</label>
    <textarea class="form-control" id="message" rows="3">%MESSAGE%</textarea>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>
